#!/bin/bash

varstocheck=(
  PASSWORD_STORE_DIR
  HCLOUD_TOKEN
)

for var in "${varstocheck[@]}"; do
  if [[ -z ${!var+x} ]];
    then echo "$var is unset, this may leads to errors"
  fi
done

echo "Merge upstream after fetch"
git stash
git fetch --all && git merge -s ort -X theirs upstream/master && git push
if [[ $(git stash list) != "" ]]; then
  git stash pop || (echo "Stash pop failed, will exit" && exit 1)
fi

echo "Update Ansible roles and collections"
ansible-galaxy install collections -f -r requirements.yml
just roles

echo "Execute Ansible now"
ansible-playbook -e "env=prod" -i inventory/hcloud.yml setup.yml --tags setup-all,start,common
